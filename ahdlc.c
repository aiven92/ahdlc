#include "ahdlc.h"
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

#define AHDLC_DEBUG_ERROR
#define AHDLC_DEBUG_WARN
//#define AHDLC_DEBUG_INFO

#if defined(__clang__) || defined(__GNUC__)
# ifndef LOGFORMAT
#   define LOGFORMAT __attribute__ ((format (__printf__, 1, 2)))
# endif
# ifndef LOGFORMATERR
#   define LOGFORMATERR __attribute__ ((format (__printf__, 2, 3)))
# endif
#else
# ifndef LOGFORMAT
#   define LOGFORMAT
# endif
# ifndef LOGFORMATERR
#   define LOGFORMATERR
# endif
#endif

const char ahdlc_logging_prefix[] = "AHDLC: ";

LOGFORMAT static inline void ahdlc_error(const char* fmt, ...)
{
#ifdef AHDLC_DEBUG_ERROR
  va_list ap;
  va_start(ap, fmt);
  printf(KRED "ERROR %s " KNRM, ahdlc_logging_prefix);
  vprintf(fmt, ap);
  fflush(stdout);
  va_end(ap);
#else
  (void) fmt;
#endif
}

LOGFORMAT static inline void ahdlc_warn(const char* fmt, ...)
{
#ifdef AHDLC_DEBUG_WARN
  va_list ap;
  va_start(ap, fmt);
  printf(KYEL "WARN  %s " KNRM, ahdlc_logging_prefix);
  vprintf(fmt, ap);
  fflush(stdout);
  va_end(ap);
#else
  (void) fmt;
#endif
}

LOGFORMAT static inline void ahdlc_info(const char* fmt, ...)
{
#ifdef AHDLC_DEBUG_INFO
  va_list ap;
  va_start(ap, fmt);
  printf(KNRM "INFO %s ", ahdlc_logging_prefix);
  vprintf(fmt, ap);
  fflush(stdout);
  va_end(ap);
#else
  (void) fmt;
#endif
}

static void ahdlc_dump_packet(char category, const uint8_t* buffer, size_t size)
{
#ifdef AHDLC_DEBUG_INFO
//   DEBUG_START_PRINT();
  ahdlc_info("%c:%4lu: ", category, size);

  const uint8_t* body_start;
  const uint8_t* body_end;
  uint16_t       protocol;
  const uint8_t* crc_start = buffer + size - 2;

//   printf("%02X", buffer[0]);
//   buffer += 1; //skip SOF

//   /* Print protocol field */
//   if (*buffer & 0x01) {
//     /* 8-bit protocol */
//     printf("%02X", buffer[0]);
//     protocol = buffer[0];
//     buffer += 1;
//   } else {
//     printf("%02X%02X", buffer[0], buffer[1]);
//     protocol = ((uint16_t)buffer[0] << 8) | buffer[1];
//
//   }



  /* Print payload */
  body_start = buffer;
  if (size && buffer) {
//     printf(" ");
    while (buffer < crc_start) {
      printf("%02X", *buffer++);
    }
  }
  body_end = buffer;

  /* Print crc */
  printf(" %02X%02X\n", crc_start[0], crc_start[1]);

//   printf(" %02X\n", crc_start[2]);
//   if (protocol == 0x0021)
//     {
//       /* IPv4 */
//       ip_packet(category, body_start, body_end - body_start);
//     }
#else
  (void)category;
  (void)buffer;
  (void)size;
#endif
}

#define AHDLC_ESCAPED        0x1

/* Frame is ready bit */

#define AHDLC_START_BYTE     0x7e
#define AHDLC_ESCAPE_CHAR    0x7d

#define AHDLC_RX_READY       0x2
#define AHDLC_RX_ASYNC_MAP   0x4
#define AHDLC_TX_ASYNC_MAP   0x8
#define AHDLC_PFC            0x10
#define AHDLC_ACFC           0x20

#define I_FRAME 0x0
#define S_FRAME 0x1
#define U_FRAME 0x2

#define LCP                 0xc021 // 0b 1100 0000 0010 0001
#define PAP                 0xc023 // 0b 1100 0000 0010 0011
#define IPCP                0x8021 // 0b 1000 0000 0010 0001
#define IPV4                0x0021 // 0b 0000 0000 0010 0001

// ????
#define CRC_GOOD_VALUE      0xf0b8

static uint16_t crcadd(uint16_t crcvalue, uint8_t c)
{
  uint16_t b;

  b = (crcvalue ^ c) & 0xFF;
  b = (b ^ (b << 4)) & 0xFF;
  b = (b << 8) ^ (b << 3) ^ (b >> 4);

  return ((crcvalue >> 8) ^ b);
}

static void ahdlc_rx_ready(struct ahdlc_s *ahdlc)
{
// #ifdef PPP_STATISTICS
//   if(ahdlc->rx_count > ahdlc->rx_count_max)
//     {
//       ahdlc->rx_count_max = ahdlc->rx_count;
//     }
// #endif
  ahdlc->rx_count = 0;
  ahdlc->crc = 0xffff;
  ahdlc->flags |= AHDLC_RX_READY;
}

void ahdlc_init(struct ahdlc_s *ahdlc,
                void* rx_buff, uint16_t rx_size,
                void* tx_buff, uint16_t tx_size,
                void* lower_handler,
                ssize_t (*lower_read )(void* lower_handler,       void *buf, size_t count),
                ssize_t (*lower_write)(void* lower_handler, const void *buf, size_t count),
                void (*lower_drain)(void* lower_handler))
{
  ahdlc->flags      = 0 | AHDLC_RX_ASYNC_MAP;
  ahdlc->rx_count   = 0;

  ahdlc->tx_buff    = tx_buff;
  ahdlc->tx_size    = tx_size;

  ahdlc->rx_buff    = rx_buff;
  ahdlc->rx_size    = rx_size;

  ahdlc->lower_handler  = lower_handler;
  ahdlc->lower_read     = lower_read;
  ahdlc->lower_write    = lower_write;
  ahdlc->lower_drain    = lower_drain;

  ahdlc_rx_ready(ahdlc);
// #ifdef PPP_STATISTICS
//   ahdlc->ahdlc_rx_tobig_error = 0;
//   ahdlc->rx_count_max   = 0;
// #endif
}



/*
 * input - input buffer pointer
 * count - input buffer size
 * output - output payload otherwise NULL
 * output_size - if output payload not NULL
 * returned:
 * >0 : value number of readed bytes from incomming buffer
 * <0 : errore code
 */
// int ahdlc_rx(struct ahdlc_s *ahdlc,
//                  const uint8_t* input, uint16_t count,
//                  uint8_t* output, uint16_t* output_size)

ssize_t ahdlc_rx(struct ahdlc_s *ahdlc, void *buf, size_t count)
{
  int ret;
  uint8_t c;

//   printf("%s\n", __FUNCTION__);

//   ret = ahdlc->lower_read(ahdlc->lower_handler, &c, 1);

  while((ret = ahdlc->lower_read(ahdlc->lower_handler, &c, 1)) > 0 )
  {

//     printf("READ 0x%02X\n", c);

    if (ahdlc->flags & AHDLC_RX_READY)
    {
      /* Check to see if character is less than 0x20 hex we really
         should set AHDLC_RX_ASYNC_MAP on by default and only turn it
         off when it is negotiated off to handle some buggy stacks. */

      if ((c < 0x20) && ((ahdlc->flags & AHDLC_RX_ASYNC_MAP) == 0))
        {
          /* Discard character */

          ahdlc_warn("Discard control char (< 0x20) and asysnc map is 0\n");
          return 0;
        }

      /* Are we in escaped mode? */

      /* Start Of Frame syncronization */
      if (ahdlc->flags & AHDLC_ESCAPED)
        {
          /* Set escaped to FALSE */

          ahdlc->flags &= ~AHDLC_ESCAPED;

          /* If value is AHDLC_START_BYTE then silently discard and reset receive packet */

          if (c == AHDLC_START_BYTE)
            {
              ahdlc_rx_ready(ahdlc);
              return 0;
            }

          /* Incoming char = itself xor 20 */

          c = c ^ 0x20;
        }
      else if (c == AHDLC_START_BYTE)
        {
          /* Handle frame end */
          if (ahdlc->rx_count > 1) {
            ahdlc_dump_packet('I', ahdlc->rx_buff, ahdlc->rx_count);
          }

          if (ahdlc->crc == CRC_GOOD_VALUE)
            {

              /*_TRACE(("AHDLC:Receiving packet with good crc value, len %d\n",
                     ahdlc->rx_count));*/

              /* we hae a good packet, turn off CTS until we are done with
                this packet */
              /*CTS_OFF();*/

#if PPP_STATISTICS
              /* Update statistics */

              ++ahdlc->ppp_rx_frame_count;
#endif

              /* Femove CRC bytes from packet */

              ahdlc->rx_count -= 2;

              /* Lock PPP buffer */

              ahdlc->flags &= ~AHDLC_RX_READY;

             /*upcall routine must fully process frame before return
              *    as returning signifies that buffer belongs to AHDLC again.
              */

//               if ((c & 0x1) && (ahdlc->flags & PPP_PFC))
//                 {
//                   /* Send up packet */
//
// //                   ppp_upcall(ahdlc, (u16_t)ahdlc->ahdlc_rx_buffer[0],
// //                       (u8_t *)&ahdlc->ahdlc_rx_buffer[1],
// //                       (u16_t)(ahdlc->rx_count - 1));
//                 }
//               else
//                 {
//                   /* Send up packet */
//
// //                   ppp_upcall(ahdlc, (u16_t)(ahdlc->ahdlc_rx_buffer[0] << 8 | ahdlc->ahdlc_rx_buffer[1]),
// //                       (u8_t *)&ahdlc->ahdlc_rx_buffer[2], (u16_t)(ahdlc->rx_count - 2));
//                 }
//               return ;
              memcpy(buf, ahdlc->rx_buff, ahdlc->rx_count);
              ret = ahdlc->rx_count;
              ahdlc_rx_ready(ahdlc);
              return ret;
            }
          else if (ahdlc->rx_count > 1)
            {
              ahdlc_warn("Drop packet with bad crc value, was 0x%04x len %d\n",
                     ahdlc->crc, ahdlc->rx_count);
#ifdef PPP_STATISTICS
              ++ahdlc->ahdlc_crc_error;
#endif
              /* Shouldn't we dump the packet and not pass it up? */

              /*ppp_upcall((u16_t)ahdlc_rx_buffer[0],
                (u8_t *)&ahdlc_rx_buffer[0], (u16_t)(rx_count+2));
                dump_ppp_packet(&ahdlc_rx_buffer[0],rx_count);*/
            }

          ahdlc_rx_ready(ahdlc);
          return 0;
        }
      else if (c == AHDLC_ESCAPE_CHAR)
        {
          /* Handle escaped chars*/

          ahdlc->flags |= AHDLC_ESCAPED;
          return 0;
        }

      /* Rry to store char if not too big */

      if (ahdlc->rx_count >= ahdlc->rx_size)
        {
#ifdef PPP_STATISTICS
          ++ahdlc->rx_tobig_error;
#endif
          ahdlc_rx_ready(ahdlc);
        }
      else
        {
          /* Add CRC in */

          ahdlc->crc = crcadd(ahdlc->crc, c);

//           /* Do auto ACFC, if packet len is zero discard 0xff and 0x03 */
//
//           if (ahdlc->rx_count == 0)
//             {
//               /* First byte can be an address, in most cases it is a broadcast address 0xFF
//                * PPP does not provide or assign individual, so it can be dropped
//                *
//                * Second byte is HDLC control field, for PPP it is always 0x03, drop it.
//                */
//               if ((c == 0xff) || (c == 0x03))
//                 {
//                   return 0;
//                 }
//             }

          /* Store char */

          ahdlc->rx_buff[ahdlc->rx_count++] = c;
        }
    }
  else
    {
      /* we are busy and didn't process the character. */
      ahdlc_warn("I:Drop byte 0x%02X, busy/not active\n", c);
      printf("drop\n");
      return -1;
    }
  }
  return ret;
}

uint8_t ahdlc_tx_char(struct ahdlc_s *ahdlc, uint16_t* crc, uint16_t protocol, uint8_t c)
{

// #ifdef AHDLC_BUFFERED
//   /* Check buffer is full */
//   if(ahdlc->buff >= ahdlc->buff_size){
//     ppp_arch_send_buff(ahdlc);
//   }
// #endif
//   printf("crc\n");
  /* Add in crc */

  if(ahdlc->tx_count + 2 >= ahdlc->tx_size)
  {
    ahdlc_error("Tx buff is full\n");
    return 0;
  }
  *crc = crcadd(*crc, c);

  /* See if we need to escape char, we always escape AHDLC_START_BYTE and AHDLC_ESCAPE_CHAR,
   * in the case
   * of char < 0x20 we only support async map of default or none, so escape if
   * ASYNC map is not set.  We may want to modify this to support a bitmap set
   * ASYNC map.
   */

  if (
         (c == AHDLC_ESCAPE_CHAR)
      || (c == AHDLC_START_BYTE)
//       || ((c < 0x20) && ((protocol == LCP) || (ahdlc->flags & AHDLC_TX_ASYNC_MAP) == 0))
    )
    {
      /* Send escape char and xor byte by 0x20 */
//       printf("esc %02X\n", AHDLC_ESCAPE_CHAR);
      ahdlc->tx_buff[ahdlc->tx_count++] = AHDLC_ESCAPE_CHAR;
//       printf("ori %02X ", c);
      c ^= 0x20;
    }

//   printf("%02X\n", c);
  ahdlc->tx_buff[ahdlc->tx_count++] = c;
  return 1;
}

static void ahdlc_buff_clear(struct ahdlc_s *ahdlc)
{
  ahdlc->tx_count = 0;
}

ssize_t ahdlc_tx(struct ahdlc_s *ahdlc, const void *buf, size_t count)
{
  uint16_t i;
//   uint8_t c;
  uint16_t protocol = 0;
  int ret = 0;
  ssize_t written = 0;

  ahdlc_buff_clear(ahdlc);
  uint16_t crc = 0xffff;

  /* Write leading AHDLC_START_BYTE */
//   printf("start %02X\n", AHDLC_START_BYTE);
  ahdlc->tx_buff[ahdlc->tx_count++] = AHDLC_START_BYTE;

  /* Write Protocol */
//   ahdlc_tx_char(ahdlc, &crc, protocol,(uint8_t)(protocol >> 8));
//   ahdlc_tx_char(ahdlc, &crc, protocol,(uint8_t)(protocol & 0xff));

  for (i = 0; i < count; ++i)
    {
      written += ahdlc_tx_char(ahdlc, &crc, protocol, *((uint8_t*)buf+i));
    }

  /* Send crc, lsb then msb */
  i = crc ^ 0xffff;
  written += ahdlc_tx_char(ahdlc, &crc, protocol, (uint8_t)(i & 0xff));
  written += ahdlc_tx_char(ahdlc, &crc, protocol, (uint8_t)((i >> 8) & 0xff));

//   printf("stop %02X\n", AHDLC_START_BYTE);
  ahdlc->tx_buff[ahdlc->tx_count++] = AHDLC_START_BYTE;;

  ret = ahdlc->lower_write(ahdlc->lower_handler, ahdlc->tx_buff, ahdlc->tx_count);
//   if(count >= 256)
    ahdlc_dump_packet('O', ahdlc->tx_buff+1, ahdlc->tx_count-2);

    // If write succece, return writen bytes(without framing), else - error code
  return ret>0 ? count : ret;

}

void ahdlc_drain(struct ahdlc_s *ahdlc){
    ahdlc->lower_drain(ahdlc->lower_handler);
}
