#pragma once
#include <stdint.h>
#include <sys/types.h>
#include <unistd.h>

#define AHDLC_BUFFERED

struct ahdlc_s
{
  int flags;
  uint8_t* rx_buff;
  uint16_t rx_count;
  uint16_t rx_size;
  uint16_t crc;

#ifdef AHDLC_BUFFERED
  uint8_t* tx_buff;
  uint16_t tx_count;
  uint16_t tx_size;
#endif

  void* lower_handler;

  ssize_t (*lower_read )(void* lower_handler,       void *buf, size_t count);
  ssize_t (*lower_write)(void* lower_handler, const void *buf, size_t count);
  ssize_t (*lower_drain)(void* lower_handler);
};

#ifdef __cplusplus
extern "C" {
#endif
  
void ahdlc_init(struct ahdlc_s *ahdlc,
                void* rx_buff, uint16_t rx_buff_size,
                void* tx_buff, uint16_t tx_buff_size,
                void* lower_handler,
                ssize_t (*lower_read )(void* lower_handler,       void *buf, size_t count),
                ssize_t (*lower_write)(void* lower_handler, const void *buf, size_t count),
                void (*lower_drain)(void* lower_handler)
);

ssize_t ahdlc_rx(struct ahdlc_s *ahdlc,       void *buf, size_t count);
ssize_t ahdlc_tx(struct ahdlc_s *ahdlc, const void *buf, size_t count);
void ahdlc_drain(struct ahdlc_s *ahdlc);
  
#ifdef __cplusplus
}
#endif
